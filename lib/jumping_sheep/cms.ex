defmodule JumpingSheep.CMS do
  @moduledoc """
  The CMS context.
  """

  import Ecto.Query, warn: false
  alias JumpingSheep.Repo

  alias JumpingSheep.CMS.{Author, Pages}
  alias JumpingSheep.Accounts

  @doc """
  Returns the list of pages.

  ## Examples

      iex> list_pages()
      [%Pages{}, ...]

  """
  def list_pages do
    Pages
    |> Repo.all()
    |> Repo.preload(author: [user: :credential])
  end

  @doc """
  Gets a single pages.

  Raises `Ecto.NoResultsError` if the Pages does not exist.

  ## Examples

      iex> get_pages!(123)
      %Pages{}

      iex> get_pages!(456)
      ** (Ecto.NoResultsError)

  """
  def get_pages!(id) do
    Pages
    |> Repo.get!(id)
    |> Repo.preload(author: [user: :credential])
  end


  @doc """
  Creates a pages.

  ## Examples

      iex> create_pages(%{field: value})
      {:ok, %Pages{}}

      iex> create_pages(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_pages(%Author{}= author, attrs \\ %{}) do
    %Pages{}
    |> Pages.changeset(attrs)
    |> Ecto.Changeset.put_change(:author_id,author.id)
    |> Repo.insert()
  end

  @doc """
  Updates a pages.

  ## Examples

      iex> update_pages(pages, %{field: new_value})
      {:ok, %Pages{}}

      iex> update_pages(pages, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_pages(%Pages{} = pages, attrs) do
    pages
    |> Pages.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a pages.

  ## Examples

      iex> delete_pages(pages)
      {:ok, %Pages{}}

      iex> delete_pages(pages)
      {:error, %Ecto.Changeset{}}

  """
  def delete_pages(%Pages{} = pages) do
    Repo.delete(pages)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking pages changes.

  ## Examples

      iex> change_pages(pages)
      %Ecto.Changeset{data: %Pages{}}

  """
  def change_pages(%Pages{} = pages, attrs \\ %{}) do
    Pages.changeset(pages, attrs)
  end

  alias JumpingSheep.CMS.Author

  @doc """
  Returns the list of authors.

  ## Examples

      iex> list_authors()
      [%Author{}, ...]

  """
  def list_authors do
    Repo.all(Author)
  end

  @doc """
  Gets a single author.

  Raises `Ecto.NoResultsError` if the Author does not exist.

  ## Examples

      iex> get_author!(123)
      %Author{}

      iex> get_author!(456)
      ** (Ecto.NoResultsError)

  """
  def get_author!(id), do: Author|> Repo.get!(id)|>Repo.preload(user: :credential)


  @doc """
  Creates a author.

  ## Examples

      iex> create_author(%{field: value})
      {:ok, %Author{}}

      iex> create_author(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_author(attrs \\ %{}) do
    %Author{}
    |> Author.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a author.

  ## Examples

      iex> update_author(author, %{field: new_value})
      {:ok, %Author{}}

      iex> update_author(author, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_author(%Author{} = author, attrs) do
    author
    |> Author.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a author.

  ## Examples

      iex> delete_author(author)
      {:ok, %Author{}}

      iex> delete_author(author)
      {:error, %Ecto.Changeset{}}

  """
  def delete_author(%Author{} = author) do
    Repo.delete(author)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking author changes.

  ## Examples

      iex> change_author(author)
      %Ecto.Changeset{data: %Author{}}

  """
  def change_author(%Author{} = author, attrs \\ %{}) do
    Author.changeset(author, attrs)
  end

  def ensure_author_exists(%Accounts.User{} = user) do
    %Author{user_id: user.id}
    |> Ecto.Changeset.change()
    |> Ecto.Changeset.change()
    |> Ecto.Changeset.unique_constraint(:user_id)
    |> Repo.insert()
    |> handle_existing_author()
  end
  def handle_existing_author({:ok, author}) do
    author
  end
  def handle_existing_author({:error, changeset}) do
    Repo.get_by(Author, user_id: changeset.data.user_id)
  end

  def inc_pages_views(%Pages{}  = page) do
    IO.inspect(page.id)
    {1, [%Pages{views: views}]} =
      from(p in Pages, where: p.id == ^page.id, select: [:views])
      |> Repo.update_all(inc: [views: 1])

    put_in(page.views, views)
  end
end
