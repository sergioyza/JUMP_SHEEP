defmodule JumpingSheep.CMS.Pages do
  use Ecto.Schema
  import Ecto.Changeset

  alias JumpingSheep.CMS.Author

  schema "pages" do
    field :body, :string
    field :title, :string
    field :views, :integer
    belongs_to :author, Author

    timestamps()
  end

  @doc false
  def changeset(pages, attrs) do
    pages
    |> cast(attrs, [:title, :body])
    |> validate_required([:title, :body])
  end
end
