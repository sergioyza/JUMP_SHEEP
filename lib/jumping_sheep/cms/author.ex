defmodule JumpingSheep.CMS.Author do
  use Ecto.Schema
  import Ecto.Changeset

  alias JumpingSheep.CMS.Pages
  alias  JumpingSheep.Accounts.User

  schema "authors" do
    field :bio, :string
    field :role, :string
    has_many :pages, Pages
    belongs_to :user, User
    timestamps()
  end

  @doc false
  def changeset(author, attrs) do
    author
    |> cast(attrs, [:bio, :role])
    |> validate_required([:bio, :role])
    |> unique_constraint(:user_id)
  end
end
