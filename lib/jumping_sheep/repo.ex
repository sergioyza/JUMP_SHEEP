defmodule JumpingSheep.Repo do
  use Ecto.Repo,
    otp_app: :jumping_sheep,
    adapter: Ecto.Adapters.Postgres
end
