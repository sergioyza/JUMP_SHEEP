defmodule JumpingSheepWeb.PageController do
  use JumpingSheepWeb, :controller

  def index(conn, _params) do
    render(conn, "index.html")
  end
end
