defmodule JumpingSheepWeb.CMS.PagesController do
  use JumpingSheepWeb, :controller

  alias JumpingSheep.CMS
  alias JumpingSheep.CMS.Pages

  plug :required_existing_author
  plug :authorize_page  when action in [:edit, :update, :delete]

  def index(conn, _params) do
    pages = CMS.list_pages()
    render(conn, "index.html", pages: pages)
  end

  def new(conn, _params) do
    changeset = CMS.change_pages(%Pages{})
    render(conn, "new.html", changeset: changeset)
  end

  def create(conn, %{"pages" => pages_params}) do
    case CMS.create_pages(conn.assigns.current_author, pages_params) do
      {:ok, pages} ->
        conn
        |> put_flash(:info, "Pages created successfully.")
        |> redirect(to: Routes.cms_pages_path(conn, :show, pages))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "new.html", changeset: changeset)
    end
  end

  def show(conn, %{"id" => id}) do
    IO.inspect(conn)
    pages = id
    |> CMS.get_pages!()
    |> CMS.inc_pages_views()
    render(conn, "show.html", pages: pages)
  end

  def edit(conn,_) do
    IO.inspect(conn)
    changeset = CMS.change_pages(conn.assigns.page)
    render(conn, "edit.html", changeset: changeset)
  end

  def update(conn, %{"pages" => pages_params}) do
    IO.inspect(conn)
    case CMS.update_pages(conn.assigns.page, pages_params) do
      {:ok, pages} ->
        conn
        |> put_flash(:info, "Pages updated successfully.")
        |> redirect(to: Routes.cms_pages_path(conn, :show, pages))

      {:error, %Ecto.Changeset{} = changeset} ->
        render(conn, "edit.html", changeset: changeset)
    end
  end

  def delete(conn, _) do
    {:ok, _pages} = CMS.delete_pages(conn.assigns.page)

    conn
    |> put_flash(:info, "Pages deleted successfully.")
    |> redirect(to: Routes.cms_pages_path(conn, :index))
  end

  defp required_existing_author(conn,_) do
    author = CMS.ensure_author_exists(conn.assigns.current_user)
    assign(conn, :current_author, author)
  end

  defp authorize_page(conn,_) do
    page = CMS.get_pages!(conn.params["id"])

    if conn.assigns.current_author.id == page.author.id do
      assign(conn,:page, page)
    else
      conn
      |> put_flash(:error, "You can't nmodify that page")
      |> redirect(to: Routes.cms_pages_path(conn, :index))
      |> halt()
    end
  end
end
