defmodule JumpingSheepWeb.CMS.PagesView do
  use JumpingSheepWeb, :view

  alias JumpingSheep.CMS

  def author_name(%CMS.Pages{author: author}) do
    author.user.name
  end
end
