# JumpingSheep

To start your Phoenix server:

  * Install dependencies with `mix deps.get`
  * Create and migrate your database with `mix ecto.setup`
  * Start Phoenix endpoint with `mix phx.server` or inside IEx with `iex -S mix phx.server`

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

Ready to run in production? Please [check our deployment guides](https://hexdocs.pm/phoenix/deployment.html).

## Learn more

  * Official website: https://www.phoenixframework.org/
  * Guides: https://hexdocs.pm/phoenix/overview.html
  * Docs: https://hexdocs.pm/phoenix
  * Forum: https://elixirforum.com/c/phoenix-forum
  * Source: https://github.com/phoenixframework/phoenix

## Context
  * Creation of users, and use of user sessions to be able to create personal pages, where they can be viewed by other users who are logged in, with some validations that only be able to edit their own pages and count of the views by pages

## How does it work?

  * http://localhost:4000/users/new     First create new user 
  * http://localhost:4000/sessions/new  Second login, the password is disabled put anything 
  * http://localhost:4000/cms/pages     List of you pages created
  * http://localhost:4000/cms/pages/new New pages with title and body

## References

  * Elxir course: playlist https://www.youtube.com/playlist?list=PLTd5ehIj0goPm2HGj-YVy-IlIfBXDL9NC 
  * Elixir book: `Elixir in Action second edition`
  * Udemy course Phoenix and ecto: https://www.udemy.com/course/elixir-phoenix-real-world-functional-programming/   
  * Ecto and Phoenix book: `Programming Phoenix Productive |> Realiable |> Fast`,
  * Certificate Ecto and Phoenix by Udemy: https://www.udemy.com/certificate/UC-85dcfba7-054f-4a46-b2cd-46452f034818/

  ## First poryect Phoenix and Ecto by Sergio Daniel Pucheta Yza