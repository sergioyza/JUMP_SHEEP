defmodule JumpingSheepWeb.CMS.PagesControllerTest do
  use JumpingSheepWeb.ConnCase

  import JumpingSheep.CMSFixtures

  @create_attrs %{body: "some body", title: "some title", views: 42}
  @update_attrs %{body: "some updated body", title: "some updated title", views: 43}
  @invalid_attrs %{body: nil, title: nil, views: nil}

  describe "index" do
    test "lists all pages", %{conn: conn} do
      conn = get(conn, Routes.cms_pages_path(conn, :index))
      assert html_response(conn, 200) =~ "Listing Pages"
    end
  end

  describe "new pages" do
    test "renders form", %{conn: conn} do
      conn = get(conn, Routes.cms_pages_path(conn, :new))
      assert html_response(conn, 200) =~ "New Pages"
    end
  end

  describe "create pages" do
    test "redirects to show when data is valid", %{conn: conn} do
      conn = post(conn, Routes.cms_pages_path(conn, :create), pages: @create_attrs)

      assert %{id: id} = redirected_params(conn)
      assert redirected_to(conn) == Routes.cms_pages_path(conn, :show, id)

      conn = get(conn, Routes.cms_pages_path(conn, :show, id))
      assert html_response(conn, 200) =~ "Show Pages"
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, Routes.cms_pages_path(conn, :create), pages: @invalid_attrs)
      assert html_response(conn, 200) =~ "New Pages"
    end
  end

  describe "edit pages" do
    setup [:create_pages]

    test "renders form for editing chosen pages", %{conn: conn, pages: pages} do
      conn = get(conn, Routes.cms_pages_path(conn, :edit, pages))
      assert html_response(conn, 200) =~ "Edit Pages"
    end
  end

  describe "update pages" do
    setup [:create_pages]

    test "redirects when data is valid", %{conn: conn, pages: pages} do
      conn = put(conn, Routes.cms_pages_path(conn, :update, pages), pages: @update_attrs)
      assert redirected_to(conn) == Routes.cms_pages_path(conn, :show, pages)

      conn = get(conn, Routes.cms_pages_path(conn, :show, pages))
      assert html_response(conn, 200) =~ "some updated body"
    end

    test "renders errors when data is invalid", %{conn: conn, pages: pages} do
      conn = put(conn, Routes.cms_pages_path(conn, :update, pages), pages: @invalid_attrs)
      assert html_response(conn, 200) =~ "Edit Pages"
    end
  end

  describe "delete pages" do
    setup [:create_pages]

    test "deletes chosen pages", %{conn: conn, pages: pages} do
      conn = delete(conn, Routes.cms_pages_path(conn, :delete, pages))
      assert redirected_to(conn) == Routes.cms_pages_path(conn, :index)

      assert_error_sent 404, fn ->
        get(conn, Routes.cms_pages_path(conn, :show, pages))
      end
    end
  end

  defp create_pages(_) do
    pages = pages_fixture()
    %{pages: pages}
  end
end
