defmodule JumpingSheep.CMSFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `JumpingSheep.CMS` context.
  """

  @doc """
  Generate a pages.
  """
  def pages_fixture(attrs \\ %{}) do
    {:ok, pages} =
      attrs
      |> Enum.into(%{
        body: "some body",
        title: "some title",
        views: 42
      })
      |> JumpingSheep.CMS.create_pages()

    pages
  end

  @doc """
  Generate a author.
  """
  def author_fixture(attrs \\ %{}) do
    {:ok, author} =
      attrs
      |> Enum.into(%{
        bio: "some bio",
        role: "some role"
      })
      |> JumpingSheep.CMS.create_author()

    author
  end
end
