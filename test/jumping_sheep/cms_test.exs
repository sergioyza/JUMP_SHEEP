defmodule JumpingSheep.CMSTest do
  use JumpingSheep.DataCase

  alias JumpingSheep.CMS

  describe "pages" do
    alias JumpingSheep.CMS.Pages

    import JumpingSheep.CMSFixtures

    @invalid_attrs %{body: nil, title: nil, views: nil}

    test "list_pages/0 returns all pages" do
      pages = pages_fixture()
      assert CMS.list_pages() == [pages]
    end

    test "get_pages!/1 returns the pages with given id" do
      pages = pages_fixture()
      assert CMS.get_pages!(pages.id) == pages
    end

    test "create_pages/1 with valid data creates a pages" do
      valid_attrs = %{body: "some body", title: "some title", views: 42}

      assert {:ok, %Pages{} = pages} = CMS.create_pages(valid_attrs)
      assert pages.body == "some body"
      assert pages.title == "some title"
      assert pages.views == 42
    end

    test "create_pages/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = CMS.create_pages(@invalid_attrs)
    end

    test "update_pages/2 with valid data updates the pages" do
      pages = pages_fixture()
      update_attrs = %{body: "some updated body", title: "some updated title", views: 43}

      assert {:ok, %Pages{} = pages} = CMS.update_pages(pages, update_attrs)
      assert pages.body == "some updated body"
      assert pages.title == "some updated title"
      assert pages.views == 43
    end

    test "update_pages/2 with invalid data returns error changeset" do
      pages = pages_fixture()
      assert {:error, %Ecto.Changeset{}} = CMS.update_pages(pages, @invalid_attrs)
      assert pages == CMS.get_pages!(pages.id)
    end

    test "delete_pages/1 deletes the pages" do
      pages = pages_fixture()
      assert {:ok, %Pages{}} = CMS.delete_pages(pages)
      assert_raise Ecto.NoResultsError, fn -> CMS.get_pages!(pages.id) end
    end

    test "change_pages/1 returns a pages changeset" do
      pages = pages_fixture()
      assert %Ecto.Changeset{} = CMS.change_pages(pages)
    end
  end

  describe "authors" do
    alias JumpingSheep.CMS.Author

    import JumpingSheep.CMSFixtures

    @invalid_attrs %{bio: nil, role: nil}

    test "list_authors/0 returns all authors" do
      author = author_fixture()
      assert CMS.list_authors() == [author]
    end

    test "get_author!/1 returns the author with given id" do
      author = author_fixture()
      assert CMS.get_author!(author.id) == author
    end

    test "create_author/1 with valid data creates a author" do
      valid_attrs = %{bio: "some bio", role: "some role"}

      assert {:ok, %Author{} = author} = CMS.create_author(valid_attrs)
      assert author.bio == "some bio"
      assert author.role == "some role"
    end

    test "create_author/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = CMS.create_author(@invalid_attrs)
    end

    test "update_author/2 with valid data updates the author" do
      author = author_fixture()
      update_attrs = %{bio: "some updated bio", role: "some updated role"}

      assert {:ok, %Author{} = author} = CMS.update_author(author, update_attrs)
      assert author.bio == "some updated bio"
      assert author.role == "some updated role"
    end

    test "update_author/2 with invalid data returns error changeset" do
      author = author_fixture()
      assert {:error, %Ecto.Changeset{}} = CMS.update_author(author, @invalid_attrs)
      assert author == CMS.get_author!(author.id)
    end

    test "delete_author/1 deletes the author" do
      author = author_fixture()
      assert {:ok, %Author{}} = CMS.delete_author(author)
      assert_raise Ecto.NoResultsError, fn -> CMS.get_author!(author.id) end
    end

    test "change_author/1 returns a author changeset" do
      author = author_fixture()
      assert %Ecto.Changeset{} = CMS.change_author(author)
    end
  end
end
